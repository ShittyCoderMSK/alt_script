#!/bin/bash

# Установка Git, если он не установлен
if ! command -v git &> /dev/null; then
    echo "Git не установлен. Установка Git..."
    sudo apt-get update && sudo apt-get install -y git
    echo "Git успешно установлен."
else
    echo "Git уже установлен."
fi

# Клонирование репозитория
REPO_URL="https://gitlab.com/ShittyCoderMSK/alt_script"
CLONE_DIR="$HOME/alt_script"

if [ ! -d "$CLONE_DIR" ]; then
    echo "Клонирование репозитория $REPO_URL..."
    git clone "$REPO_URL" "$CLONE_DIR" && echo "Репозиторий успешно склонирован."
else
    echo "Директория уже существует. Переход к запуску скрипта."
fi

# Переход в директорию репозитория и запуск script.sh
cd "$CLONE_DIR" && {
    if [ -f "generator.sh" ]; then
        echo "Запуск generator.sh..."
        chmod +x generator.sh
        ./generator.sh
    else
        echo "Файл generator.sh не найден."
    fi
}
